package com.pt.pedrorijo91.integer;

import java.lang.reflect.Field;

public class App {

    public static void main(String[] args) throws SecurityException, ClassNotFoundException, NoSuchFieldException,
            IllegalArgumentException, IllegalAccessException {

        //https://dzone.com/articles/reflection-and-integer-cache

        Field value = Integer.class.getDeclaredField("value");
        value.setAccessible(true);
        value.set(1, 2);
        System.out.println(Integer.valueOf(1));
        System.out.printf("5-4 => %d%n", 5 - 4);
        value.set(1001, 1002);
        System.out.println(Integer.valueOf(1001));

        Field v = String.class.getDeclaredField("value");
        v.setAccessible(true);
        v.set("hello!", "cheers".toCharArray());
        System.out.println("hello!");
    }
}
